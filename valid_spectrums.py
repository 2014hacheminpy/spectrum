import cv2
import os

def valid_spectrums(year):
    path = f'./spectrumImages/spectrumImages{year}'
    ok_files = []
    for file in os.listdir(path):
        if str(file)[-4:] == '.jpg':
            img = cv2.imread('./'+ path + '/' + file, 1)
            image = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
            length, width, lab = image.shape
            colors = []
            for i in range(width):
                tuple = (image[0][i][0], image[0][i][1], image[0][i][2])
                if tuple not in colors:
                    colors += [tuple]
                if len(colors) >= 10:
                    ok_files += [file]
                    break

    for file in os.listdir(path):
        if file not in ok_files:
            print('removing', file, '....')
            os.remove(path + "/" + file)
    print(ok_files)

years = [2004, 2005, 2006, 2007, 2008,2009,2010,2011, 2015]
for year in years:
    valid_spectrums(year)

