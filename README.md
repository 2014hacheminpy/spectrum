# Determination du genre d’un film à partir de ses couleurs

Bonjour vous trouverez dans ce répertoire notre travail concernant notre projet de Vision par Ordinateur et Deep Learning. Voici une explication des différents fichiers présents :

- **Link-dictionaries/** : contient les dictionnaires des films, de leurs genres et de leurs bande-annonces, triés par année
- **API_manager.py** : script permettant de récupérer les dictionnaires de films
- **Clustering.ipynb** : fichier jupyter permetant l'analyse de la différence de clusters entre une bande-annonce et son film
- **Extract_spectrum.py** : script créant les spectres depuis les dictionnaires.
- **Spectrum Keras Models.ipynb** : fichier jupyter contenant les modèles des algorithmes de deep learning utilisés
- **spectrumImages.zip** : dossier compressé contenant tous les spectres utilisés
- **valid_spectrum.py** : script éliminant tous les spectres uniformes donc non exploitables