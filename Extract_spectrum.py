#! /usr/bin/env python3
# coding: utf-8
# ydl1.py
from __future__ import unicode_literals
from threading import Thread
from queue import Queue
from ast import literal_eval
from skimage.transform import resize
import datetime
from time import time
from math import floor
import numpy as np
import youtube_dl
import cv2
import os
# import ffmpeg : imageio.plugins.ffmpeg.download()
import moviepy.editor as mp

follow = True
queue = Queue()
linkFile = './link-dictionaries/link-dictionary20172.txt' # Input dict of trailers #TODO; change path
linkDict = {}
exceptDict = {}
videoDir = './video/' # Folder to store temporarely the videos
spectrumDir = './spectrumImages/spectrumImages2017/' # Output folder to store the spectrums #TODO; change path
countDownload = 1
countSpectrum = 1


def main():
    global videoDir
    global spectrumDir
    global linkDict
    global linkFile
    global follow

    linkDict = reading(linkFile)
    TRAILER = Thread(target=downloadTrailer)
    SPECTRUM = Thread(target=createSpectrum)
    SPECTRUM.start()
    TRAILER.start()
    TRAILER.join()
    follow = False
    SPECTRUM.join()


def downloadTrailer():  # called by main()
    """Function downloading the trailer thanks to the youtube-dl library"""
    global countDownload
    global exceptDict
    ydl_opts = {'format': 'worst/worstvideo',
                'outtmpl': videoDir + '%(id)s.%(ext)s',
                'noplaylist': True,
                'nocheckcertificate': True,
                # 'max_filesize' : 10000000,
                'save_path': videoDir}
    for key in linkDict.keys():
        if not os.path.isfile(spectrumDir + linkDict[key][2] + '.jpg'):
            #time1 = datetime.datetime.now()
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                #while (datetime.datetime.now() - time1).seconds < 190:
                try:
                    ydl.download(['https://www.youtube.com/watch?v=' + linkDict[key][2]])
                    info_dict = ydl.extract_info('https://www.youtube.com/watch?v=' + linkDict[key][2], download=False)
                    video_ext = info_dict.get("ext", None)
                    max_filesize = info_dict.get('filesize', None)
                    # print("max_filesize : {}".format(max_filesize))
                    queue.put(linkDict[key][2] + '.' + video_ext)
                except Exception as e:  # catch *all* exceptions
                    exceptDict[linkDict[key][2]] = str(e)
                    """f = open("Exception-dictionnary.txt", "w")
                    f.write(str(exceptDict))
                    f.close()"""
            print("{}/{} downloads".format(countDownload, len(linkDict)))

        else:
            print(spectrumDir + linkDict[key][2] + '.jpg already exists !')
        countDownload += 1


def createSpectrum():  # called by main()
    """Function creating the spectrum from the full queue"""
    global countSpectrum
    global spectrumDir
    while (not queue.empty()) or follow:
        item = queue.get()
        imgSpectrum(videoDir + item, spectrumDir + item[:-4] + '.jpg')
        print(item[:-4])
        global countDownload
        print("{}/{} spectrum".format(countSpectrum, len(linkDict)))
        countSpectrum += 1
        try:
            os.remove(videoDir + item)

        except FileNotFoundError:
            pass
        queue.task_done()


def imgSpectrum(vidPath, spectrumOut):  # called by createSpectrum()
    """Function saving the image thanks to the spectrum array """
    start = time()
    if vidPath[-3:] != '3gp':
        res2 = tableSpectrum(resizeVideo(vidPath, vidPath))
    else:
        res2 = tableSpectrum(vidPath)
    output = cv2.cvtColor(res2, cv2.COLOR_Lab2BGR)
    cv2.imwrite(spectrumOut, output)
    print()
    print("--------------------------------------------------------")
    print("Duration for {} : {} s".format(vidPath[:-3], time() - start))
    print("--------------------------------------------------------")


def resizeVideo(vidIn, vidOut):  # called by imgSpectrum()
    """Function to resize the video in case it is too big (mp4 format)"""
    try:
        clip = mp.VideoFileClip(vidIn)
        clip_resized = clip.resize(height=36)  # Make the height 36px
        clip_resized.write_videofile(vidOut)
    except Exception as ex:
        print(str(ex))
        return vidOut
    return vidOut


def tableSpectrum(video):  # called by imgSpectrum()
    """Function creating the array for the spectrum"""
    count = 0
    ratioFrame = 6
    countFrame = 0
    vidcrop = cv2.VideoCapture(video)

    """Find if the trailer is in 4:3 or 21:9 --> borders to crop"""
    success, image = vidcrop.read()
    if success:
        cropdim = (0, image.shape[0], 0, image.shape[1])
        countcrop = 0
    while success and countcrop < 240:
        success, image = vidcrop.read()
        countcrop += 1
    if success == True:
        try:
            cropdim = crop_image_dim(image, 0)
        except cv2.error:
            cropdim = (0, image.shape[0], 0, image.shape[1])
        print("image size : {}".format(image.shape))
        print("cropped dim : {}".format(cropdim))

    """Read every images of the trailer and out the mean color of each"""
    vidcap = cv2.VideoCapture(video)
    length = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT) / ratioFrame)
    width = int(length * 3 / 16)
    res = np.zeros([width, length, 3], dtype=np.uint8)
    success, image = vidcap.read()

    while success:
        if countFrame == ratioFrame:
            countFrame = 0
            res[0][count] = meanImage(image, cropdim)
            for i in range(1, res.shape[0]):
                res[i][count] = res[0][count]
            count += 1
            if cv2.waitKey(10) == 27:
                break
        success, image = vidcap.read()
        countFrame += 1
    return res


def crop_image_dim(img, tol):  # called by tableSpectrum()
    """Find the dimensions cropped if this is a 4:3 or 21:9 trailer"""
    # tol is the tolerance : 0 to find black pixels
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    mask = img_gray > tol
    midV = floor(mask.shape[0] / 2)
    midH = floor(mask.shape[1] / 2)
    upBorder = 0
    downBorder = mask.shape[0]
    leftBorder = 0
    rightBorder = mask.shape[1]
    for i in range(1, mask.shape[0]):
        if mask[i - 1][midH] != mask[i][midH] and mask[i][midH] == True:
            if upBorder == mask[0][midH]:
                upBorder = i - 1
        if mask[i - 1][midH] != mask[i][midH] and mask[i][midH] == False:
            downBorder = i
    for j in range(1, mask.shape[1]):
        if mask[midV][j - 1] != mask[midV][j] and mask[midV][j] == True:
            if leftBorder == mask[midV][0]:
                leftBorder = j - 1
        if mask[midV][j - 1] != mask[midV][j] and mask[midV][j] == False:
            rightBorder = j

    return (upBorder, downBorder, leftBorder, rightBorder)


def meanImage(img, cropdim):  # called by tableSpectrum()
    """Return the mean LAB array only with pixels in cropped image"""
    res0 = [0, 0, 0]
    res = [0, 0, 0]
    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    for i in range(cropdim[0], cropdim[1]):
        for j in range(cropdim[2], cropdim[3]):
            res0 = res0 + lab[i][j]
    sumpix=(cropdim[1]-cropdim[0]+1)*(cropdim[3]-cropdim[2]+1)
    if sumpix == 0:
        sumpix = 1
    res0[0] = res0[0] / sumpix
    res0[1] = res0[1] / sumpix
    res0[2] = res0[2] / sumpix
    res[0] = int(res0[0])
    res[1] = int(res0[1])
    res[2] = int(res0[2])
    return res


def reading(path):  # Called by main()
    """Function to create the dictionnary from the text file"""
    with open(path, 'r') as f:
        s = f.read()
        whip = literal_eval(s)
    return whip


if __name__ == '__main__':
    main()
