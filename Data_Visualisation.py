import ast
import os
import requests
import matplotlib.pyplot as plt


genres = [28, 35, 18, 99, 10749, 10752, 10402, 53, 878, 27, 9648, 80, 14, 12, 36, 10769, 16, 10751, 37, 10770]

years = range(2000, 2018)

list_of_eligible_spectrums = []
for year in years:
    for file in os.listdir("./spectrumImages/SpectrumImages" + str(year)):
        if str(file)[-4:] == '.jpg':
            list_of_eligible_spectrums += ['SpectrumImages'+ str(year) +'/' + file]

url = "https://api.themoviedb.org/3/genre/movie/list?"
API = 'api_key=a9075982d1f7ce05cc45adec0e5f5358&language=en-US'

def get_genres(year):
    path = "./Link-dictionaries/Link-dictionary" + str(year) +".txt"
    file = open(path, "r").read()
    dictyear = ast.literal_eval(file)
    dict_genres = {}
    for movie_id in dictyear.keys():
        genre_ids = dictyear[movie_id][1]
        for genre_id in genre_ids:
            if genre_id in dict_genres.keys():
                dict_genres[genre_id] += 1
            else:
                dict_genres[genre_id] = 1
    return dict_genres

def translate_genre():
    url_final = url + API
    req = requests.get(url_final)
    print(req.json()["genres"])

def sum_genre(list_years):
    final_dict_genre = {}
    for year in list_years:
        print(f'dealing with {year}...')
        dict_genre = get_genres(year)
        for key in dict_genre.keys():
            if key in final_dict_genre.keys():
                final_dict_genre[key] += dict_genre[key]
            else:
                final_dict_genre[key] = dict_genre[key]
    return final_dict_genre

sum_genre = sum_genre(range(2000, 2018))

L = []
for key in sum_genre.keys():
    L += [str(key)]
print(L, list(sum_genre.values()))
print(sum(list(sum_genre.values())))
plt.bar(L, list(sum_genre.values()))
plt.show()