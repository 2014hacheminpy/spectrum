import requests
import datetime
import time
import ast

url = 'https://api.themoviedb.org/3/discover/movie?api_key=e4c6a6f5fbd60b0316b7ff30e73bec74&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&'

#il faut ajouter l'année après
url_get_video = "http://api.themoviedb.org/3/movie/" #157336/videos?api_key=e4c6a6f5fbd60b0316b7ff30e73bec74

#API = 'api_key=e4c6a6f5fbd60b0316b7ff30e73bec74&language=en-US'
API = 'api_key=a9075982d1f7ce05cc45adec0e5f5358&language=en-US'

def get_movie_ids(year):
    now1 = datetime.datetime.now()
    # url_final = url + 'page=1&year=' + str(year)
    # req = requests.get(url_final)
    dict_movies = {}
    nb_of_pages = 550
    # nb_of_pages = req.json()['total_pages']
    # for item in req.json()['results']:
    #     movie_id = item['id']
    #     video_link = get_video_link(movie_id)
    #     dict_movies[movie_id] = [item['vote_average'], item["genre_ids"], video_link]
    #     f = open("./Link-dictionaries/Link-dictionary" + str(year) + ".txt", "w")
    #     f.write(str(dict_movies))
    #     f.close()

    for page in range(163, nb_of_pages+1): #TODO: changer la page if needed
        time.sleep(7)
        print(dict_movies)
        print(len(dict_movies))
        print(f"taking care of page # {page}... / ", nb_of_pages)
        url_final = url + 'page=' + str(page) +'&year=' + str(year)
        req = requests.get(url_final)
        for item in req.json()['results']:
            movie_id = item['id']
            video_link = get_video_link(movie_id)
            if video_link != '':
                dict_movies[movie_id] = [item['vote_average'], item["genre_ids"], video_link]
                f = open("./Link-dictionaries/Link-dictionary" + str(year) + "-2.txt", "w")
                f.write(str(dict_movies))
                f.close()
    return dict_movies

def get_video_link(movie_id):
    url = url_get_video + str(movie_id) + '/videos?' + API
    req = requests.get(url)
    #print(req.json())
    if req.json()['results'] != []:
        if req.json()['results'][0]['site'] == 'YouTube':
            return req.json()['results'][0]['key']
    else:
        return ''


# Helpers

def concatenate_dict():
    path = "./Link-dictionaries/Link-dictionary2005-1.txt"
    file = open(path, "r").read()
    dictyear = ast.literal_eval(file)
    print(len(dictyear))
    path2 = "./Link-dictionaries/Link-dictionary2005-2.txt"
    file2 = open(path2, "r").read()
    dict2 = ast.literal_eval(file2)
    print(len(dict2), 'dict2')
    dictyear.update(dict2)
    print(len(dictyear), 'new dictyear')
    path3 = "./Link-dictionaries/Link-dictionary2005-3.txt"
    file3 = open(path3, "r").read()
    dict3 = ast.literal_eval(file3)
    print(len(dict3))
    dictyear.update(dict3)
    print(len(dictyear))
    f = open("./Link-dictionaries/Link-dictionary2005.txt", "w")
    f.write(str(dictyear))
    f.close()
    return (dictyear)

concatenate_dict()






